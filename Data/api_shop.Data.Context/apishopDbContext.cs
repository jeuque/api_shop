﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using api_shop.Data.Entity.Entities;

namespace api_shop.Data.Entity;

public partial class apishopDbContext : DbContext
{
    public apishopDbContext()
    {
    }

    public apishopDbContext(DbContextOptions<apishopDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Address> Addresses { get; set; }

    public virtual DbSet<Basket> Baskets { get; set; }

    public virtual DbSet<Category> Categories { get; set; }

    public virtual DbSet<Customer> Customers { get; set; }

    public virtual DbSet<Order> Orders { get; set; }

    public virtual DbSet<Product> Products { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseMySQL("Server=localhost;Database=api_shop;Port=3306;User Id=root;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Address>(entity =>
        {
            entity.HasKey(e => e.IdAddress).HasName("PRIMARY");

            entity.ToTable("address");

            entity.Property(e => e.IdAddress)
                .HasColumnType("int(11)")
                .HasColumnName("idAddress");
            entity.Property(e => e.Address1)
                .HasMaxLength(255)
                .HasColumnName("address");
            entity.Property(e => e.City)
                .HasMaxLength(255)
                .HasColumnName("city");
            entity.Property(e => e.ZipCode)
                .HasMaxLength(255)
                .HasColumnName("zipCode");
        });

        modelBuilder.Entity<Basket>(entity =>
        {
            entity.HasKey(e => e.IdBasket).HasName("PRIMARY");

            entity.ToTable("basket");

            entity.HasIndex(e => e.IdCustomer, "idCustomer");

            entity.HasIndex(e => e.IdProduct, "idProduct");

            entity.Property(e => e.IdBasket)
                .HasColumnType("int(11)")
                .HasColumnName("idBasket");
            entity.Property(e => e.IdCustomer)
                .HasColumnType("int(11)")
                .HasColumnName("idCustomer");
            entity.Property(e => e.IdProduct)
                .HasColumnType("int(11)")
                .HasColumnName("idProduct");
            entity.Property(e => e.QuantityProduct)
                .HasColumnType("int(11)")
                .HasColumnName("quantityProduct");

            entity.HasOne(d => d.IdCustomerNavigation).WithMany(p => p.Baskets)
                .HasForeignKey(d => d.IdCustomer)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("basket_ibfk_1");

            entity.HasOne(d => d.IdProductNavigation).WithMany(p => p.Baskets)
                .HasForeignKey(d => d.IdProduct)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("basket_ibfk_2");
        });

        modelBuilder.Entity<Category>(entity =>
        {
            entity.HasKey(e => e.IdCategory).HasName("PRIMARY");

            entity.ToTable("category");

            entity.Property(e => e.IdCategory)
                .HasColumnType("int(11)")
                .HasColumnName("idCategory");
            entity.Property(e => e.NomCategory)
                .HasMaxLength(255)
                .HasColumnName("nomCategory");
        });

        modelBuilder.Entity<Customer>(entity =>
        {
            entity.HasKey(e => e.IdCustomer).HasName("PRIMARY");

            entity.ToTable("customer");

            entity.HasIndex(e => e.IdAddress, "idAddress");

            entity.Property(e => e.IdCustomer)
                .HasColumnType("int(11)")
                .HasColumnName("idCustomer");
            entity.Property(e => e.EmailCusomer)
                .HasMaxLength(255)
                .HasColumnName("emailCusomer");
            entity.Property(e => e.FirstNameCustomer)
                .HasMaxLength(255)
                .HasColumnName("firstNameCustomer");
            entity.Property(e => e.IdAddress)
                .HasColumnType("int(11)")
                .HasColumnName("idAddress");
            entity.Property(e => e.LastNameCustomer)
                .HasMaxLength(255)
                .HasColumnName("lastNameCustomer");

            entity.HasOne(d => d.IdAddressNavigation).WithMany(p => p.Customers)
                .HasForeignKey(d => d.IdAddress)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("customer_ibfk_1");
        });

        modelBuilder.Entity<Order>(entity =>
        {
            entity.HasKey(e => e.IdOrders).HasName("PRIMARY");

            entity.ToTable("orders");

            entity.HasIndex(e => e.IdBasket, "idBasket");

            entity.Property(e => e.IdOrders)
                .HasColumnType("int(11)")
                .HasColumnName("idOrders");
            entity.Property(e => e.DateOrders)
                .HasColumnType("datetime")
                .HasColumnName("dateOrders");
            entity.Property(e => e.IdBasket)
                .HasColumnType("int(11)")
                .HasColumnName("idBasket");
            entity.Property(e => e.State)
                .HasMaxLength(255)
                .HasColumnName("state");

            entity.HasOne(d => d.IdBasketNavigation).WithMany(p => p.Orders)
                .HasForeignKey(d => d.IdBasket)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("orders_ibfk_1");
        });

        modelBuilder.Entity<Product>(entity =>
        {
            entity.HasKey(e => e.IdProduct).HasName("PRIMARY");

            entity.ToTable("product");

            entity.HasIndex(e => e.IdCategory, "idCategory");

            entity.Property(e => e.IdProduct)
                .HasColumnType("int(11)")
                .HasColumnName("idProduct");
            entity.Property(e => e.IdCategory)
                .HasColumnType("int(11)")
                .HasColumnName("idCategory");
            entity.Property(e => e.NameProduct)
                .HasMaxLength(255)
                .HasColumnName("nameProduct");
            entity.Property(e => e.PrixProduct).HasColumnName("prixProduct");
            entity.Property(e => e.StockProduct)
                .HasColumnType("int(11)")
                .HasColumnName("stockProduct");

            entity.HasOne(d => d.IdCategoryNavigation).WithMany(p => p.Products)
                .HasForeignKey(d => d.IdCategory)
                .OnDelete(DeleteBehavior.Restrict)
                .HasConstraintName("product_ibfk_1");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
