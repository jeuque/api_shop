﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Customer
{
    public int IdCustomer { get; set; }

    public string? FirstNameCustomer { get; set; }

    public string? LastNameCustomer { get; set; }

    public string? EmailCusomer { get; set; }

    public int? IdAddress { get; set; }

    public virtual ICollection<Basket> Baskets { get; } = new List<Basket>();

    public virtual Address? IdAddressNavigation { get; set; }
}
