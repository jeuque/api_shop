﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Category
{
    public int IdCategory { get; set; }

    public string? NomCategory { get; set; }

    public virtual ICollection<Product> Products { get; } = new List<Product>();
}
