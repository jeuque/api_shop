﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Product
{
    public int IdProduct { get; set; }

    public string? NameProduct { get; set; }

    public double? PrixProduct { get; set; }

    public int? StockProduct { get; set; }

    public int? IdCategory { get; set; }

    public virtual ICollection<Basket> Baskets { get; } = new List<Basket>();

    public virtual Category? IdCategoryNavigation { get; set; }
}
