﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Basket
{
    public int IdBasket { get; set; }

    public int? IdCustomer { get; set; }

    public int? IdProduct { get; set; }

    public int? QuantityProduct { get; set; }

    public virtual Customer? IdCustomerNavigation { get; set; }

    public virtual Product? IdProductNavigation { get; set; }

    public virtual ICollection<Order> Orders { get; } = new List<Order>();
}
