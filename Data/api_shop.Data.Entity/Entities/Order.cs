﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Order
{
    public int IdOrders { get; set; }

    public DateTime? DateOrders { get; set; }

    public int? IdBasket { get; set; }

    public string? State { get; set; }

    public virtual Basket? IdBasketNavigation { get; set; }
}
