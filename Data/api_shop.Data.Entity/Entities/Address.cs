﻿using System;
using System.Collections.Generic;

namespace api_shop.Data.Entity.Entities;

public partial class Address
{
    public int IdAddress { get; set; }

    public string? Address1 { get; set; }

    public string? ZipCode { get; set; }

    public string? City { get; set; }

    public virtual ICollection<Customer> Customers { get; } = new List<Customer>();
}
